###adicionar os volumes /etc/puppetlabs/code/environments/production/manifests/
###adicionar os volumes /etc/hosts
FROM centos:7
MAINTAINER FSC
LABEL Vendor="FSC" 
      

RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install httpd ntpdate openssh-server vim  && \
	rpm -Uvh https://yum.puppetlabs.com/puppet5/puppet5-release-el-7.noarch.rpm \
	yum --setopt=tsflags=nodocs install -y puppetserver \
	yum clean all 

RUN	ntp 0.centos.pool.ntp.org \
	ntpdate 0.centos.pool.ntp.org \
	hostname puppet.fsc.com

ADD	puppetserver /etc/sysconfig/ \
    puppet.conf /etc/puppetlabs/puppet/puppet.conf

RUN systemctl start puppetserver \
	systemctl enable puppetserver

			
EXPOSE 22 80 8140

